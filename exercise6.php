<?php

    $host = 'localhost';
    $username = 'root';
    $password = '';
    $database = 'p8_exercise_backend';


    $conn = new mysqli($host, $username, $password, $database);

    if($conn->connect_error){
        die("Connection failed: " . $conn->connect_error);
    }


    //retrieve the first name, last name, and birthday of all employees in the table
   


    //Write a SQL query to insert a new employee with the first name 'Jane', last name 'Doe', middle name 'Marie', birthday '1990-01-01', and address '456 Elm Street'.
      $sql = "INSERT INTO employee (first_name, last_name, middle_name, birthday, address) VALUES('Jane', 'Doe', 'Marie', '1990-01-1', '456 Elm Street')";

    if ($conn->query($sql) === TRUE){
        echo "New record created successfully.  <br>";
    }
    else{
        echo "Error" . $sql . "<br>" . $conn->error . " <br>";
    }  

    
    //Write a SQL query to retrieve the first name, last name, and birthday of all employees in the table.
    $sql = 'SELECT * FROM employee'; 

    $result = $conn->query($sql);

    if($result->num_rows > 0){
        while($row = $result->fetch_assoc()){
            echo "First Name: " . $row["first_name"] . " - Last Name: " . $row["last_name"] . " - Birthday: " . $row["birthday"] . "<br>";
        }
       
    }
    else{
        echo "0 results  <br>";
    }
    
    


    
    //Write a SQL query to retrieve the number of employees whose last name starts with the letter 'D'.
    $sql = "SELECT * FROM employee WHERE last_name LIKE 'D%'";

    $result = $conn->query($sql);

    if($result->num_rows > 0){
        echo ($result->num_rows) . " <br>";
    }
    else{
        echo "0 results  <br>";
    }
    




    //Write a SQL query to retrieve the first name, last name, and address of the employee with the highest ID number.
    $sql = "SELECT first_name, last_name, address FROM employee ORDER BY id DESC LIMIT 1; ";

    $result = $conn->query($sql);

    if($result->num_rows > 0){
        while($row = $result->fetch_assoc()){
            echo "First Name: " . $row["first_name"] . " - Last Name: " . $row["last_name"] . " - Birthday: " . $row["address"] . "<br>";
        }
    }
    else{
        echo "0 results  <br>";
    }

   



     //Write a SQL query to update the address of the employee with the first name 'John' to '123 Main Street'.
     $sql = 'UPDATE employee SET address="123 Main Street" WHERE first_name = "John"';

     if($conn->query($sql) === TRUE){
        echo "Record updated successfully  <br>";
     }
     else{
         echo "Error updating record: " . $conn->error . " <br>";
     }


    

    
      //Write a SQL query to delete all employees whose last name starts with the letter 'D'.
       $sql = "DELETE FROM employee WHERE last_name LIKE 'D%'" ;

      if($conn->query($sql) === TRUE){
         echo "Record deleted successfully <br>";
      }
      else{
          echo "Error updating record: " . $conn->error . " <br>";
      } 
 

      $conn->close

?>