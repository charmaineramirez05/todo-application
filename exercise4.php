<?php

    class API{

        public $age;
        public $emailaddress;
        public $birthday;

        function printFullName(string $fullName){
            echo ("Full Name: $fullName <br>");
        }

        function printHobbies(array $hobbies){
            echo("Hobbies: <br>");
            foreach ($hobbies as $hobby){
                echo("&nbsp; $hobby<br>");
            }
        }

        function printDetails(API $apiparam){
            echo("Age: $apiparam->age <br>");
            echo("Email: $apiparam->emailaddress <br>");
            echo("Birthday: $apiparam->birthday <br>"); 
        }
    }


    $apiObject = new API();

    $apiObject->age = 21;

    $apiObject->emailaddress = "charmaineramirez05@gmail.com";

    $apiObject->birthday = "May 11, 2002"; 


    $apiObject->printFullName("Charmaine Mae I. Ramirez");

    $apiObject->printHobbies(["Cooking", "Gardening", "Listening to music", "Watching anime shows", "Watching K-variety shows"]); 

    $apiObject->printDetails($apiObject);

?>